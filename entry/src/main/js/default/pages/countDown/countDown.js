export default {
    props: {
        mSec: {  // 毫秒
            type: Number,
            default: 0,
        },
        sec: {  // 秒
            type: Number,
            default: 0,
        },
        minute: {  // 分钟
            type: Number,
            default: 0,
        },
        hour: {  // 小时
            type: Number,
            default: 0,
        },
        targetTime:{   // 目标时间 和上年的不能共用
            type: String,
            default:''
        },
        fromTime:{  // 开始时间
            type:String,
            default:''
        },

        millisecond:{  // 毫秒级倒计时，未完成
            type:Boolean,
            default:false
        },
        control:{
            type:Boolean,
            default:false
        },
        autoStart:{
            type:Boolean,
            default:false
        },
        showAll:{
            type:Boolean,
            default:false
        }
    },

    data:{
        countDownTime:0,  // 倒计时 毫秒数
        timer:null,  // 倒计时 定时器
        countDownTimeText:'',  // 倒计时 字符串
        hourTime:0,  // 小时
        minuteTime:0,  // 分钟
        secondTime:0,   // 秒
        delay:1000,   // 定时器延时
        initTime:0,  // 初始化

        startTimeText:'', // 开始时间文本
        startTime:0,  // 开始时间
        stopTimeText:'',  // 结束时间文本
        stopTime:0,   // 结束时间

        order:false
    },

    onInit() {
        this.countDown()
        this.targetCountDown()
        if (this.autoStart) {
            this.startTiming()
        }

        if(this.millisecond){
            this.delay = 30
//            result = ":" + this.sizeFormat(millisecond) + "" + result;
        }
    },

    // 普通模式
    countDown(){
        if(this.mSec||this.sec||this.minute||this.hour){
            this.countDownTime = parseInt(this.countDownTime)
                                +parseInt(this.mSec)
                                +parseInt(this.sec)*1000
                                +parseInt(this.minute)*1000*60
                                +parseInt(this.hour)*1000*60*60
            this.initTime = this.countDownTime
            this.timeToString(this.countDownTime)
        }
    },

    // 目标时间 模式(和普通模式不通用)
    targetCountDown(){
        if(this.targetTime){
            let currentTime = Date.parse(new Date())
            let targetTime = Date.parse(new Date(this.targetTime))
            if (this.fromTime) {
                currentTime = Date.parse(new Date(this.fromTime))
            }
            this.countDownTime = targetTime-currentTime>0?targetTime-currentTime:currentTime-targetTime
            this.initTime = this.countDownTime
            this.timeToString(this.countDownTime)
        }
    },

    // 按钮控制模块
    start(){
        clearInterval(this.timer)
        this.startTiming()
    },
    pause(){
        clearInterval(this.timer)
    },
    reset(){
        this.countDownTime = this.initTime
        this.order = false
        this.timeToString(this.countDownTime)
        clearInterval(this.timer)
    },

    // 时间选择器模块
    startSelect(e){
        this.startTime = 0
                        +parseInt(e.second)*1000
                        +parseInt(e.minute)*1000*60
                        +parseInt(e.hour)*1000*60*60
        this.startTimeText = `${this.sizeFormat(e.hour)}:${this.sizeFormat(e.minute)}:${this.sizeFormat(e.second)}`
        this.stopToStart()

    },
    stopSelect(e){
        this.stopTime =  0
                        +parseInt(e.second)*1000
                        +parseInt(e.minute)*1000*60
                        +parseInt(e.hour)*1000*60*60
        this.stopTimeText = `${this.sizeFormat(e.hour)}:${this.sizeFormat(e.minute)}:${this.sizeFormat(e.second)}`;
        this.stopToStart()
    },
    stopToStart(){
        if(this.startTime&&this.stopTime){
            clearInterval(this.timer)
            this.countDownTime = this.stopTime-this.startTime>0?this.stopTime-this.startTime:this.countDownTime
            this.initTime = this.countDownTime
            this.timeToString(this.countDownTime)
        }
    },

    // 定时器开启
    startTiming(){
        if (this.countDownTime && this.order == false) {
            this.timer = setInterval(()=>{
                this.timeToString(this.countDownTime);
                this.countDownTime = this.countDownTime - this.delay

                if(this.countDownTime<=0){ // 时间归零停止定时器
                    this.countDownTimeText = '00:00:00'
                    clearTimeout(this.timer)
                }
            },this.delay)
        }else {
            this.order = true
            this.timer = setInterval(()=>{
                this.timeToString(this.countDownTime);
                this.countDownTime = this.countDownTime + this.delay
            },this.delay)
        }
    },

    // 毫秒 转换成 字符串
    timeToString(value) { // 毫秒时间转换
        let secondTime = parseInt(value / 1000), // 秒
            minuteTime = 0, // 分
            hourTime = 0, // ⼩时
            millisecond = Math.floor((value % 1000)/10)
        if (secondTime > 60) { //如果秒数⼤于60，将秒数转换成整数
            minuteTime = parseInt(secondTime / 60);//获取分钟，除以60取整数，得到整数分钟
            secondTime = parseInt(secondTime % 60);//获取秒数，秒数取佘，得到整数秒数
            if (minuteTime > 60) {//如果分钟⼤于60，将分钟转换成⼩时
                hourTime = parseInt(minuteTime / 60);//获取⼩时，获取分钟除以60，得到整数⼩时
                minuteTime = parseInt(minuteTime % 60);//获取⼩时后取佘的分，获取分钟除以60取佘的分
            }
        }

        let result = ""

        if(this.millisecond){
            this.delay = 30
            result = ":" + this.sizeFormat(millisecond) + "" + result;
        }
        result = "" + this.sizeFormat(secondTime) + "" + result;

        if (minuteTime >= 0) {
            result = "" + this.sizeFormat(minuteTime) + ":" + result;
        }

        if (hourTime >= 0) {
            result = "" + this.sizeFormat(hourTime) + ":" + result;
        }

        this.countDownTimeText = result
        this.hourTime = hourTime
        this.minuteTime = minuteTime
        this.secondTime = secondTime

        return result;
    },

    sizeFormat(value){
        let val = parseInt(value)
        return val>=10?''+val:'0'+val
    },
    onDestroy(){
        clearInterval(this.timer)
    },
}
